class Complex:
    re = 0.0
    im = 0.0

    def __init__(self, re, im):
        self.re = re
        self.im = im
        pass

    def __str__(self):
        if self.im < 0:
            return str(self.re) + str(self.im) + "j"
        elif self.im > 0:
            return str(self.re) + "+" + str(self.im) + "j"
        else:
            return str(self.re)

    @staticmethod
    def add(c1, c2):
        return Complex(c1.re + c2.re, c1.im + c2.im)

    pass


com1 = Complex(2, 6)
com2 = Complex(-5, -6)

print(Complex.add(com1, com2))
